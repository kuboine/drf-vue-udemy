Vue.component('comment-list', {
  props: {
    comments: {
      type: Array,
      required: true
    }
  },
  data() {
    return {
      new_comment: null,
      comment_author: null,
      error: null,
    }
  },
  methods: {
    submitComment() {
      if (this.comment_author && this.new_comment) {
        this.$emit('submit-comment', { username: this.comment_author, content: this.new_comment })
        this.comment_author = null
        this.new_comment = null
        if (this.error) {
          this.error = null
        }
      } else {
        this.error = "All fields are required."
      }
    }
  },
  template: `
    <div class="mt-2 mb-4">
      <single-comment v-for="(comment, index) in comments"
               :comment="comment"
               :key="index"
               >
      </single-comment>

      <hr>

      <h4>{{ error }}</h4>
      <form @submit.prevent="submitComment">
        <div class="form-group">
          <label for="commentAuthor">Your Username</label>
          <input id="commentAuthor"
                 class="form-control" 
                 type="text" 
                 v-model="comment_author">
        </div>
        <div class="form-group">
          <label for="commentText">Add a comment</label>
          <textarea id="commentText" 
                    class="form-control" 
                    cols="40" 
                    rows="3" 
                    v-model="new_comment">
          </textarea>
        </div>
        <button class="btn btn-sm btn-primary" type="submit">Publish</button>
      </form>
    </div>
  `
})


Vue.component('single-comment', {
  props: {
    comment: {
      type: Object,
      required: true
    }
  },
  template: `
    <div class="mb-3">
      <div class="card">
        <div class="card-header">
          <p>Published by: {{ comment.username }}</p>
        </div>
        <div class="card-body">
          <p>{{ comment.content }}</p>
        </div>
      </div>
    </div>
  `
})

const app = new Vue({
  el: '#app',
  data: {
    comments: [
      { username: 'charleen', content: 'hello guys' },
      { username: 'marco', content: 'hello guys' },
      { username: 'champ', content: 'hello guys' },
      { username: 'joe', content: 'hello guys' },
      { username: 'brad', content: 'hello guys' },
    ]
  },
  methods: {
    addNewComment(new_comment) {
      this.comments.push(new_comment)
    }
  }
})
