const app = new Vue({
  el: '#app',
  data: {
    first_name: 'John',
    last_name: 'Doe'
  },
  computed: {
    getRandomComputed() {
      return Math.random()
    },
    fullName() {
      return `${ this.first_name } ${ this.last_name }`
    },
    reversedFullName() {
      return `${ this.first_name } ${ this.last_name }`.split('').reverse().join('')
    }
  },
  methods: {
    getRandomNumber() {
      return Math.random()
    }
  }
})
