Vue.component('comment', {
  props: {
    comment: {
      type: Object,
      required: true
    }
  },
  template: `
    <div>
      <div class="card-body">
        <p>{{ comment.username }}</p>
        <p>{{ comment.content }}</p>
      </div>
    </div>
  `
})

const app = new Vue({
  el: '#app',
  data: {
    comments: [
      { username: 'charleen', content: 'hello guys' },
      { username: 'marco', content: 'hello guys' },
      { username: 'champ', content: 'hello guys' },
      { username: 'joe', content: 'hello guys' },
      { username: 'brad', content: 'hello guys' },
    ]
  }
})
